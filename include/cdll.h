#include <kernel.h>

#define CDLL_HEAD_INIT(name)                                                   \
  {                                                                            \
    &(name), &(name)                                                           \
  }

#define CDLL_HEAD(name) struct cdll_node name = CDLL_HEAD_INIT(name)

#define cdll_entry(ptr, type, member) container_of(ptr, type, member)

#define cdll_for_each(pos, head)                                               \
  for (pos = (head)->next; pos != (head); pos = pos->next)

#define cdll_for_each_entry(pos, head, member)                                 \
  for (pos = cdll_entry((head)->next, __typeof__(*pos), member);               \
       &pos->member != (head);                                                 \
       pos = cdll_entry(pos->member.next, __typeof__(*pos), member))

struct cdll_node
{
  struct cdll_node *prev, *next;
};

static inline void
INIT_CDLL_HEAD(struct cdll_node* head)
{
  head->next = head;
  head->prev = head;
}

static inline void
list_add(struct cdll_node* node, struct cdll_node* head)
{
  head->next->prev = node;
  node->next = head->next;
  node->prev = head;
  // https://stackoverflow.com/questions/34988277
  head->next = node;
}
