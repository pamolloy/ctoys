#include <stddef.h>

// See c7acec713d14c6ce8a20154f9dfda258d6bcad3b
#define container_of(ptr, type, member)                                        \
  ({                                                                           \
    void* __mptr = (void*)(ptr);                                               \
    ((type*)(__mptr - offsetof(type, member)));                                \
  })
