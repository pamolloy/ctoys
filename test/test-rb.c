#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define STACK_SIZE (1024 * 1024)
#define BUFFER_SIZE 1024
#define BUFFER_VALUE 255

static int
child(void* arg)
{
  uint8_t(*buf)[BUFFER_SIZE] = arg;

  (*buf)[0] = BUFFER_VALUE;

  return 0;
}

int
main()
{
  uint8_t buf[BUFFER_SIZE] = { 0 };

  // Skip malloc() and pass NULL as the starting address to mmap() so the
  // kernel chooses the page-aligned address
  char* stack = mmap(NULL,
                     STACK_SIZE,
                     PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK,
                     -1,
                     0);
  if (stack == MAP_FAILED) {
    fprintf(stderr, "Failed to allocate child stack: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // Assume stack grows downward so provide the end of the stack
  int pid = clone(child, stack + STACK_SIZE, CLONE_VM | SIGCHLD, buf);
  if (pid == -1) {
    fprintf(stderr, "Failed to clone child: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  if (waitpid(pid, NULL, 0) == -1) {
    fprintf(stderr,
            "Failed to wait for child %i to exit: %s\n",
            pid,
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  assert(buf[0] == BUFFER_VALUE);

  exit(EXIT_SUCCESS);
}
