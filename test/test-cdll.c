#include "cdll.h"
#include <stdint.h>
#include <stdio.h>

struct foo
{
  uint64_t count;
  struct cdll_node node;
};

int
main()
{
  struct foo one = { 1, CDLL_HEAD_INIT(one.node) };

  struct foo two;
  two.count = 2;
  INIT_CDLL_HEAD(&two.node);

  CDLL_HEAD(counters);

  list_add(&one.node, &counters);
  list_add(&two.node, &counters);

  struct foo* f = NULL;
  cdll_for_each_entry(f, &counters, node)
  {
    printf("Foo(next=%p, prev=%p, count=%lu)\n",
           f->node.next,
           f->node.prev,
           f->count);
  }

  return 0;
}
